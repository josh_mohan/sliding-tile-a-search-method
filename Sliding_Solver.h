/**
@file Board_Tile.h 
@brief 
@author Josh Moan
@author Felix Schiel
@author Dawson Meyer
*/

#ifndef Sliding_Solver_H
#define Sliding_Solver_H
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <list>
#include <queue>
#include "Board_Tile.h"
struct comparator{
//   bool operator()(const std::pair<int, Board_Tile> &left, const std::pair<int, Board_Tile> &right){
   bool operator()(pair<int, Board_Tile> left, pair<int, Board_Tile> right){
   return left.first > right.first;
   }
};

class Sliding_Solver{

  private:
   std::priority_queue<pair<int, Board_Tile>,vector<pair<int, Board_Tile>>, comparator> priorityboard;
   //std::priority_queue<pair<int, Board_Tile>,vector<pair<int, Board_Tile>>, comparator> tempPriority;
   // std::list<Board_Tile> temp;
   std::list<Board_Tile> allconfigs;
   string startconfig;
   string goalconfig;
   int Adist(Board_Tile& B);


  public:

   Sliding_Solver( std::string& s, std::string& g);

   void Solve_Puzzle(Sliding_Solver s);


};
#endif
