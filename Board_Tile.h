
#ifndef BOARD_TILE_H
#define BOARD_TILE_H
#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <cmath>

using namespace std;

class Board_Tile{
   
//   int row=3;
   // int col=3;
   char board[3][3];


  
  public:
   string config;
   string moves_from_start;
   ///
   Board_Tile(const std::string& s);
   Board_Tile(const std::string& s, std::string mfs);
   
   ///
   ~Board_Tile();
   
   ///
   std::list<Board_Tile> nextConfigs(Board_Tile& s);
   
   ///
   int numMoves();
   
   ///
   int Manhattan_Distance(const Board_Tile& goal);
   
   ///
   void printBoard();

   ///
   const  int findvalX(char f) const;

   ///
   const  int findvalY(char f) const;

   // string BoardToString();
   bool operator==(Board_Tile& b);
   char& operator()(int i, int j);
   const char& operator()(int i, int j) const;
   bool operator<(Board_Tile& B);
   // Board_Tile operator=(const Board_Tile& b);
      
};

#endif
