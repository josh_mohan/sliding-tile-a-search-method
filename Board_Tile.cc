

#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <cmath>
#include "Board_Tile.h"

struct bCompare{

   bool operator()(Board_Tile left, Board_Tile right){
         std::string fin("123456780");
	 return (left.Manhattan_Distance(fin)+left.numMoves()) <
	    (right.Manhattan_Distance(fin)+right.numMoves());
   }
};


/// Constructor
Board_Tile::Board_Tile(const std::string& s): config(s), moves_from_start("")
{
   int count=0;
   for(int i=0; i<3; i++){
      for(int j=0; j<3; j++){
	 board[i][j]=s[count];
	 count++;
      }
   }  
    
}
Board_Tile::Board_Tile(const std::string& s, std::string mfs): config(s), moves_from_start(mfs)
{
   int count=0;
   
   for(int i=0; i<3; i++){
      for(int j=0; j<3; j++){
	 board[i][j]=s[count];
	 count++;
      }
   }  
    
   /*
     row=b.row;
     col=b.col;					
     board=new char*[row];
     for(int i=0; i<row; i++)
     {
     board[i]= new char[col];
     for(int j=0; j<col; j++)
     {
     for(int j=0; j<col; j++)
     (*this)(i,j)=b(i,j);
     }
     }*/
}
	 


/// Destructor
Board_Tile::~Board_Tile(){
//this deletes really bad- segfaults

}


///
std::list<Board_Tile> Board_Tile::nextConfigs(Board_Tile& s){
   std::list<Board_Tile> nextMoves;
   int x=s.findvalX('0');
   int y=s.findvalY('0');
   string tempconfig=config;
   switch(x){
      case 0:
	 switch(y){
	    case 0:
	       swap(tempconfig[0], tempconfig[1]);
	       nextMoves.push_back(Board_Tile(tempconfig, s.moves_from_start+"R"));
	       swap(tempconfig[0], tempconfig[1]);
	       swap(tempconfig[0], tempconfig[3]);
	       nextMoves.push_back(Board_Tile(tempconfig, s.moves_from_start+"D"));
	       swap(tempconfig[0], tempconfig[3]);
	       break;
	    case 1:
	       swap(tempconfig[1], tempconfig[0]);
	       nextMoves.push_back(Board_Tile(tempconfig, s.moves_from_start+"L"));
	       swap(tempconfig[1], tempconfig[0]);
	       swap (tempconfig[1], tempconfig[2]);
	       nextMoves.push_back(Board_Tile(tempconfig, s.moves_from_start+"R"));
	       swap (tempconfig[1], tempconfig[2]); swap(tempconfig[1], tempconfig[4]);
	       nextMoves.push_back(Board_Tile(tempconfig, s.moves_from_start+"D"));
	       swap(tempconfig[1], tempconfig[4]);
	       break;
	    case 2:
	       swap(tempconfig[2], tempconfig[1]);
	       nextMoves.push_back(Board_Tile(tempconfig, s.moves_from_start+"L"));
	       swap(tempconfig[2], tempconfig[1]); swap(tempconfig[2], tempconfig[5]);
	       nextMoves.push_back(Board_Tile(tempconfig, s.moves_from_start+"D"));
	       swap(tempconfig[2], tempconfig[5]);
	       break;
	 }
	 break;
      case 1:
	 switch(y){
	    case 0:
	       swap(tempconfig[3], tempconfig[0]);
	       nextMoves.push_back(Board_Tile(tempconfig, s.moves_from_start+"U"));
	       swap(tempconfig[3], tempconfig[0]); swap(tempconfig[3], tempconfig[4]);
	       nextMoves.push_back(Board_Tile(tempconfig, s.moves_from_start+"R"));
	       swap(tempconfig[3], tempconfig[4]);
	       swap(tempconfig[3], tempconfig[6]);
	       nextMoves.push_back(Board_Tile(tempconfig, s.moves_from_start+"D"));
	       swap(tempconfig[3], tempconfig[6]);
	       break;
	    case 1:
	       swap(tempconfig[4], tempconfig[1]);
	       nextMoves.push_back(Board_Tile(tempconfig, s.moves_from_start+"U"));
	       swap(tempconfig[4], tempconfig[1]); swap(tempconfig[4], tempconfig[5]);
	       nextMoves.push_back(Board_Tile(tempconfig, s.moves_from_start+"R"));
	       swap(tempconfig[4], tempconfig[5]); swap(tempconfig[4], tempconfig[7]);
	       nextMoves.push_back(Board_Tile(tempconfig, s.moves_from_start+"D"));
	       swap(tempconfig[4], tempconfig[7]); swap(tempconfig[4], tempconfig[3]);
	       nextMoves.push_back(Board_Tile(tempconfig, s.moves_from_start+"L"));
	       swap(tempconfig[3], tempconfig[4]);
	       break;
	    case 2:
	       swap(tempconfig[5], tempconfig[2]);
	       nextMoves.push_back(Board_Tile(tempconfig, s.moves_from_start+"U"));
	       swap(tempconfig[5], tempconfig[2]); swap(tempconfig[5], tempconfig[8]);
	       nextMoves.push_back(Board_Tile(tempconfig, s.moves_from_start+"D"));
	       swap(tempconfig[5], tempconfig[8]); swap(tempconfig[5], tempconfig[4]);
	       nextMoves.push_back(Board_Tile(tempconfig, s.moves_from_start+"L"));
	       swap(tempconfig[5], tempconfig[4]);
	       break;
	 }
	 break;
      case 2:
	 switch(y){
	    case 0:
	       swap(tempconfig[6], tempconfig[3]);
	       nextMoves.push_back(Board_Tile(tempconfig, s.moves_from_start+"U"));
	       swap(tempconfig[6], tempconfig[3]); swap(tempconfig[6], tempconfig[7]);
	       nextMoves.push_back(Board_Tile(tempconfig, s.moves_from_start+"R"));
	       swap(tempconfig[6], tempconfig[7]);
	       break;
	    case 1:
	       swap(tempconfig[7], tempconfig[4]);
	       nextMoves.push_back(Board_Tile(tempconfig, s.moves_from_start+"U"));
	       swap(tempconfig[7], tempconfig[4]); swap(tempconfig[7], tempconfig[8]);
	       nextMoves.push_back(Board_Tile(tempconfig, s.moves_from_start+"R"));
	       swap(tempconfig[7], tempconfig[8]); swap(tempconfig[7], tempconfig[6]);
	       nextMoves.push_back(Board_Tile(tempconfig, s.moves_from_start+"L"));
	       swap(tempconfig[7], tempconfig[6]);
	       break;
	    case 2:
	       swap(tempconfig[5], tempconfig[8]);
	       nextMoves.push_back(Board_Tile(tempconfig, s.moves_from_start+"U"));
	       swap(tempconfig[5], tempconfig[8]); swap(tempconfig[8], tempconfig[7]);
	       nextMoves.push_back(Board_Tile(tempconfig, s.moves_from_start+"L"));
	       break;
	 }
	 break;
   }


   nextMoves.sort(bCompare());
   for(std::list<Board_Tile>::iterator it= nextMoves.begin(); it!= nextMoves.end(); ++it)
	it->printBoard();
   return nextMoves;
}

///
int Board_Tile::numMoves()
{return moves_from_start.size();}
   
///
int Board_Tile::Manhattan_Distance(const Board_Tile& goal){
   int manhat=0;
   for(int i=0;i<3;i++){
      for(int j=0;j<3;j++){
	 char temp=((*this).board[i][j]);
	 int tempI=goal.findvalX(temp);
	 int tempJ=goal.findvalY(temp);
	 //int tempI2= findvalX(((*this).board[i][j]))
	    
	    if(!(board[i][j]=='0'))
	    {
	       manhat+=(abs(i-tempI)+abs(j-tempJ));
	    }
      }
   }

   return manhat;
}



//helper functions

void Board_Tile::printBoard(){
   string f ("123456780");
   for(int i=0; i<3; i++){
      cout<<endl;
      for(int j=0; j<3; j++){
	 cout<<board[i][j]<<" ";
      }
      // cout<<(*this).BoardToString();
   }
   cout<<(*this).moves_from_start<<(*this).Manhattan_Distance(f)+(*this).numMoves();
   cout<<endl;
}

const  int Board_Tile::findvalX(char f) const
{  //  if(f=='0')
   // return 0;
   for(int i=0; i<3; i++)
      for(int j=0;j<3;j++)
	 if(board[i][j]==f){
	    return i;
	 }
   return 0;
}

const  int Board_Tile::findvalY(char f) const
   {  //  if(f=='0')
      // return 0;
      for(int i=0; i<3; i++)
	   for(int j=0;j<3;j++)
	     if(board[i][j]==f){
		return j;
	     }
      return 0;
   }


//
//operators //currently don't use either () operators
bool Board_Tile::operator==(Board_Tile& left)
{
   if(this->config==left.config)
      return true;
   return false;

}
char& Board_Tile::operator()(int i, int j)
{
   return board[i][j];
}
const char& Board_Tile::operator()(int i, int j) const
{
   return board[i][j];
}
bool Board_Tile::operator<(Board_Tile& B)
{
   return Manhattan_Distance((*this)) < Manhattan_Distance(B);
}


/*
Board_Tile Board_Tile::operator=(const Board_Tile& b)
{
   row=b.row;
   col=b.col;
   char newboard=new char [row];
   for(int i=0; i<row; i++)
   {
      newboard[i]=new char[col];
      for(int j=0; j<col; j++)
	 (*this)(i,j)=b(i,j);
   }
   return *this;
}
*/

